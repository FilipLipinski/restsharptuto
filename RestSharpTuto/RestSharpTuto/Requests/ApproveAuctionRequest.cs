﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpTuto.Requests
{
    public class ApproveAuctionRequest
    {
        public string AuctionId { get; set; }
    }
}
