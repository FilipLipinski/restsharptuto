﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpTuto
{
    public class UpdateLotRequest: CreateLotRequest
    {
        public string LotId { get; set; }
    }
}
