﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpTuto
{
    public class CreateLotRequest
    {
        public string LotNumber { get; set; }

        public string LongDescription { get; set; }

        public string Title { get; set; }

        public Guid AuctionId { get; set; }

        public DateTime? EndTimeUtc { get; set; }

        public int? Quantity { get; set; }

        public decimal? LowEstimate { get; set; }

        public decimal? HighEstimate { get; set; }

        public decimal? Reserve { get; set; }

        public decimal? OpeningPrice { get; set; }
    }
}
