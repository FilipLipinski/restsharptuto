﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpTuto.Requests
{
    public class CreateAuctionRequest
    {
        public int? LegacyId { get; set; }
        public DateTime? AuctionCreatedDateTimeUtc { get; set; }
        public bool? TimeIsAlreadyUtc { get; set; }
        public string CreatedByUser { get; set; }
        public string Title { get; set; }
        public Guid ClientId { get; set; }
        public List<TestAuctionListing> AuctionListings { get; set; }
        public string TimezoneId { get; set; }
        public bool CardRequired { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string TownCity { get; set; }
        public string CountyStateName { get; set; }
        public string Postcode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
        public int? PaddleSeed { get; set; }
        public TestApprovalType ApprovalType { get; set; }
        public TestAuctionApprovalSettings ApprovalRules { get; set; }
        public string ImportantInformation { get; set; }
        public string Terms { get; set; }
        public string ShippingInfo { get; set; }
        public string TelephoneNumber { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string ConfirmationEmail { get; set; }
        public string RegistrationEmail { get; set; }
        public string PaymentReceivedEmail { get; set; }
        public bool RequestConfirmationEmail { get; set; }
        public bool RequestRegistrationEmail { get; set; }
        public bool RequestPaymentReceivedEmail { get; set; }
        public string IncrementSetName { get; set; }
        public Decimal MinimumDeposit { get; set; }
        public bool AutomaticDeposit { get; set; }
        public bool AutomaticRefund { get; set; }
        public Decimal? VatRate { get; set; }
        public Decimal BuyersPremiumVatRate { get; set; }
        public Decimal InternetSurchargeVatRate { get; set; }
        public Decimal? BuyersPremium { get; set; }
        public Decimal? InternetSurchargeRate { get; set; }
        public Decimal? BuyersPremiumCeiling { get; set; }
        public Decimal? InternetSurchargeCeiling { get; set; }
        public string WinnersNotificationNote { get; set; }
        public string TimedStart { get; set; }
        public string TimedFirstLotEnds { get; set; }
        public List<TestAuctionDate> SaleDates { get; set; }
        public List<TestAuctionDate> ViewingDates { get; set; }
        public List<TestAuctionCardTypesAndFees> AuctionCardTypes { get; set; }
        public bool PieceMeal { get; set; }
        public TestLiveAppType? LiveAppType { get; set; }
        public bool PublishPostSaleResults { get; set; }
        public Decimal? InternationalDebitCardFixedFee { get; set; }
        public Decimal? InternationalDebitCardPercentageFee { get; set; }
        public List<string> InternationalDebitCardFeeExcludedCountryList { get; set; }
        public bool ProjectedSpendRequired { get; set; }
        public List<Guid> LinkedAuctions { get; set; }
        public Decimal? AtgCommission { get; set; }
        public Decimal? AtgCommissionCeiling { get; set; }
        public string ClientsAuctionId { get; set; }
        public string HammerExcess { get; set; }
        public bool? AdvancedTimedBiddingEnabled { get; set; }
        public string BiddingType { get; set; }
    }

    public class TestAuctionListing
    {
        public string PlatformCode { get; set; }
        public TestAuctionTypeAndListing AuctionTypeAndListing { get; set; }
        public string CategoryName { get; set; }
        public bool Private { get; set; }
    }

    public enum TestAuctionTypeAndListing
    {
        CatalogueLive = 1,
        Live = 2,
        Timed = 3,
        CatalogueTimed = 4,
        Tender = 5
    }
    public enum TestApprovalType
    {
        Manual = 0,
        Automatic = 1
    }

    public class TestAuctionApprovalSettings
    {
        public bool Avs3DSecureRuleEnabled { get; set; }
        public bool AvsRuleEnabled { get; set; }
        public bool Secure3DRuleEnabled { get; set; }
        public bool NotBlockedBidderRuleEnabled { get; set; }
        public bool VerifiedEmailAddressRuleEnabled { get; set; }
        public bool VerifiedTelephoneNumberRuleEnabled { get; set; }
        public bool NotInternationalBidderRuleEnabled { get; set; }
        public string AllowedCountryCodes { get; set; }

        public TestAuctionApprovalSettings()
        {
            AllowedCountryCodes = "";
        }

    }

    public class TestAuctionDate
    {
        public DateTime Starts { get; set; }
        public DateTime? Ends { get; set; }
    }

    public class TestAuctionCardTypesAndFees
    {
        public TestCardType CardType { get; set; }
        public decimal? PercentageFee { get; set; }
        public decimal? FixedFee { get; set; }
        public TestCardUsage CardAllowedType { get; set; }
    }

    public enum TestCardType
    {
        NotSet = 0,
        MasterCard = 3,
        VISA = 6,
        VisaDebit = 8,
        MasterCardDebit = 10,
        AmericanExpress = 12,
        JCB = 14,
        Discover = 16
    }

    public enum TestCardUsage
    {
        RegistrationAndPayment,
        OnlyRegistration,
        OnlyPayment
    }
    public enum TestLiveAppType
    {
        GAP,
        SoftGlobe,
        Flash
    }
}
