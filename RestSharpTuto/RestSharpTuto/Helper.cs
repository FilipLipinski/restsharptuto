﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using RestSharpTuto.Requests;

namespace RestSharpTuto
{
    public static class Helper
    {
        public static List<TestAuctionDate> AuctionDatesFromString(string dates, bool includeEndDate)
        {
            return dates?.Split(';').Select(x => new TestAuctionDate { Starts = x.ToDateTime(), Ends = includeEndDate ? x.ToDateTime().Date.AddMinutes(1439) : (DateTime?)null }).ToList();
        }
        public static DateTime ToDateTime(this string value)
        {
            return Parse(value) ?? default(DateTime);
        }

        private static DateTime? Parse(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return default(DateTime?);
            }

            var regex = new Regex(@"(?<part>(today|now|utc|yesterday|tomorrow))($|\s*(?<sign>(\+|-))\s*(?<interval>\d+)\s*(?<period>(days|months|years|hours|minutes|seconds)))");
            var match = regex.Match(date);
            if (match.Success)
            {
                DateTime baseDate = DateTime.UtcNow;
                switch (match.Groups["part"].Value)
                {
                    case "today":
                        baseDate = DateTime.Today;
                        break;
                    case "now":
                        baseDate = DateTime.Now;
                        break;
                    case "yesterday":
                        baseDate = DateTime.Today.AddDays(-1);
                        break;
                    case "tomorrow":
                        baseDate = DateTime.Today.AddDays(1);
                        break;
                }

                if (match.Groups["sign"].Success == false)
                {
                    return baseDate;
                }

                int sign = match.Groups["sign"].Value == "+" ? 1 : -1;
                var interval = int.Parse(match.Groups["interval"].Value) * sign;

                switch (match.Groups["period"].Value)
                {
                    case "seconds": return baseDate.AddSeconds(interval);
                    case "minutes": return baseDate.AddMinutes(interval);
                    case "hours": return baseDate.AddHours(interval);
                    case "days": return baseDate.AddDays(interval);
                    case "months": return baseDate.AddMonths(interval);
                    default:
                        return baseDate.AddYears(interval);
                }
            }

            return DateTime.Parse(date);
        }
    }

    
}
