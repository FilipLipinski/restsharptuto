﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestSharpTuto
{
    class CreateAuctionResponse
    {
        public string AuctionId { get; set; }
    }
}
