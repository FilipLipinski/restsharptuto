﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using RestSharpTuto.Requests;

namespace RestSharpTuto
{
    [TestClass]
    public class UnitTest1
    {
        private static RestClient _client;

        [TestInitialize]
        public void BeforeTest()
        {
            RestClient client = new RestClient("https://qa-api.globalauctionplatform.com/")
            {
                Authenticator = new HttpBasicAuthenticator("test@atgmedia.com", "Password")
            };
            _client = client;
        }

        [TestMethod]
        public void TestHello()
        {
            RestRequest request = new RestRequest("Test/hello", Method.POST);
            request.AddJsonBody(new TestHello{FirstName = "qwe", LastName = "asd"});

            var restResponse = _client.Execute(request);

            Console.WriteLine(restResponse.Content);
        }

        [TestMethod]
        public void CreateMultipleAuctions()
        {
            for (int auctionNumber = 1; auctionNumber <= 20; auctionNumber++)
            {
                string auctionTypeString = "Competitive";
                TestAuctionTypeAndListing auctionType = TestAuctionTypeAndListing.Live;
                string auctionTitle = $"AutoClose auction{auctionNumber}";

                string auctionId = CreateAuction(auctionTypeString, auctionTitle, auctionType);

                ApproveAuction(auctionId);

                #region CreateLots
                for (int lotNumber = 1; lotNumber <= 300; lotNumber++) //set how many lots you need
                {
                    CreateLot(auctionId, lotNumber.ToString());
                }
                #region Lot2

                //RestRequest createLotRequest2 = new RestRequest("Lot/CreateLot/", Method.POST);
                //createLotRequest2.AddJsonBody(new CreateLotRequest
                //{
                //    LotNumber = "2",
                //    LongDescription = "Lot number two",
                //    Title = "Lot Two",
                //    AuctionId = Guid.Parse(deserializedCreateAuctionResponse.AuctionId),
                //    EndTimeUtc = Helper.AuctionDatesFromString("tomorrow", false)[0].Ends,
                //    Quantity = 1,
                //    LowEstimate = 300,
                //    HighEstimate = 730,
                //    Reserve = 350,
                //    OpeningPrice = 140
                //});
                //var createLotResponse2 = _client.Execute(createLotRequest2);
                //Assert.IsTrue(createLotResponse2.StatusCode == HttpStatusCode.OK);
                //var createLotResponseSerialized2 = serializer.Deserialize<CreateLotResponse>(createLotResponse2);

                #endregion
                #endregion

                PublishAuction(auctionId);
                PushAuctionLive(auctionId);
            }

        }

        private string CreateAuction(string auctionTypeString, string auctionTitle, TestAuctionTypeAndListing auctionType)
        {
            RestRequest createAuctionRequest = new RestRequest("Auction/CreateAuction/", Method.POST);
            createAuctionRequest.AddJsonBody(
                new CreateAuctionRequest
                {
                    CreatedByUser = "SimpleAuctionCreator",
                    Title = $"{auctionTypeString} {DateTime.Today.Day}.{DateTime.Today.Month}. - {auctionTitle}",
                    ClientId = new Guid("60aeabc9-ed11-45f4-a86b-a48300b30d74"), //The Harlequin Auction - sanjauct
                    AuctionListings = new List<TestAuctionListing>
                    {
                        new TestAuctionListing
                        {
                            PlatformCode = "SR", AuctionTypeAndListing = auctionType,
                            CategoryName = "Clocks, Watches & Jewellery", Private = false
                        },
                        new TestAuctionListing
                        {
                            PlatformCode = "IB", AuctionTypeAndListing = auctionType,
                            CategoryName = "Clocks, Watches & Jewellery", Private = false
                        },
                        new TestAuctionListing
                        {
                            PlatformCode = "BS", AuctionTypeAndListing = auctionType,
                            CategoryName = "Clocks, Watches & Jewellery", Private = false
                        },
                        new TestAuctionListing
                        {
                            PlatformCode = "BSC", AuctionTypeAndListing = auctionType,
                            CategoryName = "Clocks, Watches & Jewellery", Private = false
                        },
                        new TestAuctionListing
                        {
                            PlatformCode = "HA", AuctionTypeAndListing = auctionType,
                            CategoryName = "Clocks, Watches & Jewellery", Private = false
                        },
                        new TestAuctionListing
                        {
                            PlatformCode = "LT", AuctionTypeAndListing = auctionType,
                            CategoryName = "Clocks, Watches & Jewellery", Private = false
                        }
                    },
                    TimezoneId = "GMT Standard Time",
                    CardRequired = false,
                    Address1 = "Test address",
                    //Address2 = "",
                    //Address3 = "",
                    //Address4 = "",
                    TownCity = "TestTown",
                    CountyStateName = null,
                    Postcode = "1123 5813 - TestCode",
                    Country = "United Kingdom",
                    CountryCode = "UK",
                    Currency = "GBP",
                    PaddleSeed = null,
                    ApprovalType = TestApprovalType.Manual,
                    ApprovalRules = null,
                    ImportantInformation = $"{auctionTitle} - important info",
                    Terms = null,
                    ShippingInfo = null,
                    TelephoneNumber = "+44 (0)1279 817778",
                    Website = "test.me.com",
                    Email = "test@email.com",
                    ConfirmationEmail = "test@email.com",
                    RegistrationEmail = "test@email.com",
                    PaymentReceivedEmail = "test@email.com",
                    RequestConfirmationEmail = false,
                    RequestRegistrationEmail = false,
                    RequestPaymentReceivedEmail = false,
                    IncrementSetName = "10",
                    MinimumDeposit = 0,
                    AutomaticDeposit = false,
                    AutomaticRefund = false,
                    BuyersPremium = 1,
                    BuyersPremiumVatRate = 2,
                    InternetSurchargeRate = 3,
                    InternetSurchargeVatRate = 4,
                    WinnersNotificationNote = null,
                    TimedStart = $"{DateTime.Today.Year}-{DateTime.Today.Month}-{DateTime.Today.Day} 05:00:00.000000",
                    TimedFirstLotEnds =
                        $"{DateTime.Today.Year}-{DateTime.Today.Month}-{DateTime.Today.Day} 21:00:00.000000",
                    SaleDates = auctionType == TestAuctionTypeAndListing.Timed ? null : Helper.AuctionDatesFromString("today", false),//use only for live auctions
                    ViewingDates = null,
                    AuctionCardTypes = null,
                    PieceMeal = false,
                    PublishPostSaleResults = true,
                    InternationalDebitCardFeeExcludedCountryList = null,
                    ProjectedSpendRequired = false,
                    LinkedAuctions = null,
                    LiveAppType = auctionType == TestAuctionTypeAndListing.Live
                        ? TestLiveAppType.GAP
                        : (TestLiveAppType?)null,
                    AtgCommission = 4,
                    VatRate = 5,
                    AdvancedTimedBiddingEnabled = true,
                    HammerExcess = null,
                    BiddingType = "Autobids"
                });

            var createAuctionResponse = _client.Execute(createAuctionRequest);
            Assert.IsTrue(createAuctionResponse.StatusCode == HttpStatusCode.OK);

            return new JsonSerializer().Deserialize<CreateAuctionResponse>(createAuctionResponse).AuctionId;
        }

        private void CreateLot(string auctionId, string lotNumber)
        {
            RestRequest createLotRequest = new RestRequest("Lot/CreateLot/", Method.POST);
            createLotRequest.AddJsonBody(new CreateLotRequest
            {
                LotNumber = lotNumber.ToString(),
                LongDescription = $"Lot number {lotNumber}",
                Title = $"Lot {lotNumber}",
                AuctionId = Guid.Parse(auctionId),
                EndTimeUtc = Helper.AuctionDatesFromString("tomorrow", false)[0].Ends,
                Quantity = 1,
                LowEstimate = 20,
                HighEstimate = 50,
                Reserve = 20,
                OpeningPrice = 10
            });
            var createLotResponse = _client.Execute(createLotRequest);
            Assert.IsTrue(createLotResponse.StatusCode == HttpStatusCode.OK);
            //var createLotResponseSerialized = serializer.Deserialize<CreateLotResponse>(createLotResponse);
        }

        private void ApproveAuction(string auctionId)
        {
            RestRequest approveAuctionRequest = new RestRequest("auction/ApproveAuction/", Method.POST);
            approveAuctionRequest.AddJsonBody(
                new ApproveAuctionRequest
                {
                    AuctionId = auctionId
                });
            var auctionApproveResponse = _client.Execute(approveAuctionRequest);
            Assert.IsTrue(auctionApproveResponse.StatusCode == HttpStatusCode.OK);
        }

        private void PublishAuction(string auctionId)
        {
            RestRequest publishAuctionRequest = new RestRequest("Auction/PublishAuction/", Method.POST);
            var watch = System.Diagnostics.Stopwatch.StartNew();
            publishAuctionRequest.AddJsonBody(
                new ApproveAuctionRequest
                {
                    AuctionId = auctionId
                });
            var auctionPublishResponse = _client.Execute(publishAuctionRequest);
            Assert.IsTrue(auctionPublishResponse.StatusCode == HttpStatusCode.OK);
            watch.Stop();
            Console.WriteLine($"Auction {auctionId} published - publish time ~{watch.ElapsedMilliseconds / 1000} seconds");
        }

        private void PushAuctionLive(string auctionId)
        {
            RestRequest pushAuctionLiveRequest = new RestRequest("Auction/PushLive/", Method.POST);
            pushAuctionLiveRequest.AddJsonBody(
                new ApproveAuctionRequest
                {
                    AuctionId = auctionId
                });
            var pushAuctionLiveResponse = _client.Execute(pushAuctionLiveRequest);
            Assert.IsTrue(pushAuctionLiveResponse.StatusCode == HttpStatusCode.OK);
        }

        private void UpdateLot(string lotId)
        {
            //RestRequest updateLotRequest = new RestRequest("Lot/UpdateLot/", Method.PUT);
            //updateLotRequest.AddJsonBody(new UpdateLotRequest
            //{
            //    LotId = createLotResponseSerialized.LotId,
            //    LotNumber = "1",
            //    LongDescription = "Lot number one - edited",
            //    Title = "Lot One",
            //    AuctionId = Guid.Parse(createAuctionResponse.AuctionId),
            //    EndTimeUtc = Helper.AuctionDatesFromString("tomorrow", false)[0].Ends,
            //    Quantity = 1,
            //    LowEstimate = 20,
            //    HighEstimate = 50,
            //    Reserve = 20,
            //    OpeningPrice = 10
            //});
            //var updateLotResponse = _client.Execute(updateLotRequest);
            //Assert.IsTrue(updateLotResponse.StatusCode == HttpStatusCode.OK);
        }

        private void DeleteLot(string lotId)
        {
            //RestRequest deleteLotRequest = new RestRequest("Lot/DeleteLot/", Method.DELETE);
            //deleteLotRequest.AddJsonBody(new CreateLotResponse
            //{
            //    LotId = createLotResponseSerialized2.LotId
            //});
            //Assert.IsTrue(_client.Execute(deleteLotRequest).StatusCode == HttpStatusCode.OK);
        }
        private void UpdateAuction(string auctionId)
        {
            /*
            RestRequest updateAuctionRequest = new RestRequest("Auction/UpdateAuction/", Method.POST);
            updateAuctionRequest.AddJsonBody(new CreateAuctionRequest
            {
                CreatedByUser = "SimpleAuctionCreator",
                Title = $"{auctionTypeString} {DateTime.Today.Day}.{DateTime.Today.Month}. - {auctionTitle}",
                ClientId = new Guid("60aeabc9-ed11-45f4-a86b-a48300b30d74"), //The Harlequin Auction - sanjauct
                AuctionListings = new List<TestAuctionListing>
                {
                    new TestAuctionListing
                    {
                        PlatformCode = "SR", AuctionTypeAndListing = auctionType,
                        CategoryName = "Clocks, Watches & Jewellery", Private = false
                    },
                    new TestAuctionListing
                    {
                        PlatformCode = "IB", AuctionTypeAndListing = auctionType,
                        CategoryName = "Clocks, Watches & Jewellery", Private = false
                    },
                    new TestAuctionListing
                    {
                        PlatformCode = "BS", AuctionTypeAndListing = auctionType,
                        CategoryName = "Clocks, Watches & Jewellery", Private = false
                    },
                    new TestAuctionListing
                    {
                        PlatformCode = "BSC", AuctionTypeAndListing = auctionType,
                        CategoryName = "Clocks, Watches & Jewellery", Private = false
                    },
                    new TestAuctionListing
                    {
                        PlatformCode = "HA", AuctionTypeAndListing = auctionType,
                        CategoryName = "Clocks, Watches & Jewellery", Private = false
                    },
                    new TestAuctionListing
                    {
                        PlatformCode = "LT", AuctionTypeAndListing = auctionType,
                        CategoryName = "Clocks, Watches & Jewellery", Private = false
                    }
                },
                TimezoneId = "GMT Standard Time",
                CardRequired = false,
                Address1 = "Test address",
                //Address2 = "",
                //Address3 = "",
                //Address4 = "",
                TownCity = "TestTown",
                CountyStateName = null,
                Postcode = "1123 5813 - TestCode",
                Country = "United Kingdom",
                CountryCode = "UK",
                Currency = "GBP",
                PaddleSeed = null,
                ApprovalType = TestApprovalType.Manual,
                ApprovalRules = null,
                ImportantInformation = $"{auctionTitle} - important info",
                Terms = null,
                ShippingInfo = null,
                TelephoneNumber = "+44 (0)1279 817778",
                Website = "test.me.com",
                Email = "test@email.com",
                ConfirmationEmail = "test@email.com",
                RegistrationEmail = "test@email.com",
                PaymentReceivedEmail = "test@email.com",
                RequestConfirmationEmail = false,
                RequestRegistrationEmail = false,
                RequestPaymentReceivedEmail = false,
                IncrementSetName = "10",
                MinimumDeposit = 0,
                AutomaticDeposit = false,
                AutomaticRefund = false,
                BuyersPremium = 1,
                BuyersPremiumVatRate = 2,
                InternetSurchargeRate = 3,
                InternetSurchargeVatRate = 4,
                WinnersNotificationNote = null,
                TimedStart = $"{DateTime.Today.Year}-{DateTime.Today.Month}-{DateTime.Today.Day} 05:00:00.000000",
                TimedFirstLotEnds =
                    $"{DateTime.Today.Year}-{DateTime.Today.Month}-{DateTime.Today.Day} 21:00:00.000000",
                SaleDates = Helper.AuctionDatesFromString("tomorrow", false),
                ViewingDates = null,
                AuctionCardTypes = null,
                PieceMeal = false,
                PublishPostSaleResults = true,
                InternationalDebitCardFeeExcludedCountryList = null,
                ProjectedSpendRequired = false,
                LinkedAuctions = null,
                LiveAppType = auctionType == TestAuctionTypeAndListing.Live
                    ? TestLiveAppType.GAP
                    : (TestLiveAppType?) null,
                AtgCommission = 4,
                VatRate = 5,
                AdvancedTimedBiddingEnabled = true,
                HammerExcess = null,
                BiddingType = "Autobids"
            });
            */
        }
    }

    public class TestHello
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }

}
